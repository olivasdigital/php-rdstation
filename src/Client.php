<?php

namespace OlivasDigital\RdStation;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client as GuzzleHttpClient;

class Client
{
    protected string $url;

    public function __construct(
        string $url,
        protected string $clientId,
        protected string $clientSecret,
        protected string $refreshToken,
    ) {
        $this->url = $this->buildApiUrl($url);
    }

    /**
     * Realiza a autenticação na API utilizando o client_id, client_secret e refresh_token.
     */
    public function authenticate(): array
    {
        $bodyParams = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'refresh_token' => $this->refreshToken
        ];

        $request = $this->buildRequest(method: 'POST', endpoint: 'auth/token', data: $bodyParams);

        return $this->sendRequest($request);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $endpoint, string $token = '', array $pathParams = [], array $queryParams = [])
    {
        $headers = $token
            ? ['Authorization' => "Bearer $token"]
            : [];

        $request = $this->buildRequest(method: 'GET', endpoint: $endpoint, pathParams: $pathParams, queryParams: $queryParams, headers: $headers);

        return $this->sendRequest($request);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post(string $endpoint, string $token = '', array $pathParams = [], array $queryParams = [], array $data = [])
    {
        $headers = $token
            ? ['Authorization' => "Bearer $token"]
            : [];

        $request = $this->buildRequest(method: 'POST', endpoint: $endpoint, pathParams: $pathParams, queryParams: $queryParams, headers: $headers, data: $data);

        return $this->sendRequest($request);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function patch(string $endpoint, string $token = '', array $pathParams = [], array $queryParams = [], array $data = []): array
    {
        $headers = $token
            ? ['Authorization' => "Bearer $token"]
            : [];

        $request = $this->buildRequest(method: 'PATCH', endpoint: $endpoint, pathParams: $pathParams, queryParams: $queryParams, headers: $headers, data: $data);

        return $this->sendRequest($request);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $endpoint, string $token = '', array $pathParams = [], array $queryParams = []): array
    {
        $headers = $token
            ? ['Authorization' => "Bearer $token"]
            : [];

        $request = $this->buildRequest(method: 'DELETE', endpoint: $endpoint, pathParams: $pathParams, queryParams: $queryParams, headers: $headers);

        return $this->sendRequest($request);
    }

    protected function buildApiUrl(string $url): string
    {
        if (str_ends_with($url, '/')) {
            return $url;
        }

        return "$url/";
    }

    protected function buildRequest(string $method, string $endpoint, array $pathParams = [], array $queryParams = [], array $headers = [], array $data = []): Request
    {
        if (!empty($pathParams)) {
            $endpoint = str_ends_with($endpoint, '/')
                ? $endpoint
                : "$endpoint/";

            $endpoint .= $this->buildPathParams($pathParams);
        }

        if (!empty($queryParams)) {
            $endpoint = "$endpoint?" . http_build_query($queryParams);
        }

        $copyHeaders = array_change_key_case($headers, CASE_LOWER);

        if (!array_key_exists('accept', $copyHeaders)) {
            $headers['Accept'] = 'application/json';
        }

        if (!array_key_exists('content-type', $copyHeaders)) {
            $headers['Content-Type'] = 'application/json';
        }

        unset($copyHeaders);


        $body = empty($data)
            ? null
            : json_encode($data);

        return new Request($method, "{$this->url}$endpoint", $headers, $body);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendRequest(Request $request): array
    {
        $response = (new GuzzleHttpClient)->send($request);

        return json_decode((string) $response->getBody(), true);
    }

    protected function buildPathParams(array $params): string
    {
        $parts = [];

        foreach($params as $key => $value) {
            $parts[] = urlencode($key) . ':' . urlencode($value);
        }

        return implode('/', $parts);
    }
}
